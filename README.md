# README #

This is a coupled photochemistry-climate code. This model leverages
the work by the Kasting, Zahnle, and Catling groups and represents an
an effort to merge the various versions and features of the codes
that have been developed over the years by the groups using the code. Ashley Horan and Shawn Domagal-Goldman, using the past work of Antigona Segura as a guide, coupled the modern versions of photochem and clima together.

The model development was supported by NASA Astrobiology Institute's  Virtual Planetary Laboratory lead team, supported by NASA under cooperative agreement NNH05ZDA001C.

### [Documentation](https://bitbucket.org/themartians/atmos/wiki)###
